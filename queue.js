let collection = [];

// Function to print queue elements
function print() {
  console.log(collection);
  return collection;
}

// Function to enqueue an element
function enqueue(element) {
  collection = [...collection,element];
  return collection;
}

// Function to dequeue an element
function dequeue() {
	
	let newCollection = [];
	for (let i = 1; i < collection.length; i++) {
	  newCollection[i - 1] = collection[i];
	}
  
	collection = newCollection;
  
	console.log(collection);
	return collection;
  }

// Function to get the front element of the queue
function front() {
  let frontElement = collection[0];
  console.log(frontElement);
  return frontElement;
}

// Function to get the size of the queue
function size() {
	console.log(collection);
  return collection.length;
}

// Function to check if the queue is empty
function isEmpty() {
	console.log(collection);
  return collection.length === 0;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
